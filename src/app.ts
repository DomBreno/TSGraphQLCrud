import 'reflect-metadata'
import { createConnection, getConnectionOptions } from 'typeorm'
import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import { buildSchema } from 'type-graphql'

import { HelloResolver } from './resolvers/Hello.resolver'
import { MovieResolver } from './resolvers/Movie.resolver'

import { port } from './config'

class App {
  public app: express.Application

  public constructor() {
    this.app = express()
  }

  public async bootstrap(): Promise<void> {
    await this.setup(this.app)

    this.app.listen(port, () =>
      console.log(`\n> Server started: http://localhost:${port}/graphql\n`)
    )
  }

  private async setup(app: express.Application) {
    const options = await getConnectionOptions(
      process.env.NODE_ENV || 'development'
    )

    await createConnection({ ...options, name: 'default' })

    const apolloServer = new ApolloServer({
      schema: await buildSchema({
        resolvers: [HelloResolver, MovieResolver],
        validate: true
      }),
      context: ({ req, res }) => ({ req, res })
    })

    apolloServer.applyMiddleware({ app, cors: false })
  }
}

export default new App()
