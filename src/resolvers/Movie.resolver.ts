import { Resolver, Mutation, Arg, Int, Query } from 'type-graphql'
import { Movie } from '../entity/Movie'

@Resolver()
export class MovieResolver {
  @Mutation(() => Boolean)
  async createMovie(
    @Arg('title', () => String) title: string,
    @Arg('minutes', () => Int) minutes: number
  ) {
    Movie.insert({ title, minutes })
    return true
  }

  @Query(() => [Movie])
  async movies() {
    return Movie.find()
  }
}
