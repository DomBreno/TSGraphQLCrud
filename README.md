# TS GraphQL Crud

_A simple Typescript GraphQL Crud :)_

## Installation

```bash
  $ yarn || npm i
  $ touch database.sqlite
```

## Running it

```bash
      # Compiled output
  $ yarn start || npm start

      # In Development
  $ yarn start:dev || npm run start:dev
```

### Running at: http://localhost:3333/graphql
